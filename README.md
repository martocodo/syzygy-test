This is a developer test solution for the following test:
https://bitbucket.org/syzygy-idev/senior-idev-test

Notes:
I am happy that by choosing a responsive design with mobile view css enhancements, it was not necessary to create the separate mobile view and I believe my code demonstrates all the required points on the test.

Contact:
Martin Lewis
mlewisdesigner@gmail.com
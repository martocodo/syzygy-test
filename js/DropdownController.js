/* Dropdown controller - to manage to selections and product lists */
DropdownController = function(Model) {
	/* Declare vars */
	this.model;
	this.listWrapEl;
	this.resultWrapEl;
	this.activeList = [];

	/* Initial functions and params setting */
	this.init = function(Model) {
		// Set elements
		this.model 			= Model;
		this.listWrapEl 	= $(this.model.listWrapId);
		// Add our html elements
		this.addLists();
		// Add required data from model
		this.addDataToLists();
		// Bind our events to custom functions
		this.bindEvents();
	};

	/* Construct list elements */
	this.addLists = function() {

		for (i=1; i<=this.model.totalLists; i++) {
			var html = '<div class="col"><span class="ident">' + i + '</span><select id="list_' + i + '" class="' + this.model.listClass + '"></select></div>';
			this.listWrapEl.append(html);
		};

	};

	/* Add data to lists */
	this.addDataToLists = function() {
		// Set default value
		var optionsHTML = '<option value="">Select</option>';
		// First loop through items and build select options
		for (i=0; i<this.model.items.length; i++) {
			// Get item
			var item = this.model.items[i];
			// Check active flag for inclusion
			if (item.active == true) {
				optionsHTML += '<option value="' + i + '">' + item.title + '</option>';
			}
		}

		// Now add options to each list
		for (i=1; i<=this.model.totalLists; i++) {
			// Set options
			$('#list_' + i).html(optionsHTML);
			// Set Active List with blank array position for each list
			this.activeList.push('');
		}
	}

	/* Refresh list data */
	this.refreshListData = function() {
		// Loop through each list
		for (i=1; i<=this.model.totalLists; i++) {
			// First clear old actives
			$("#list_" + i + " option").removeAttr("disabled");
			// Loop through active list
			for (j=0; j<this.model.totalLists; j++) {
				// Avoid blocking select.. value
				if (this.activeList[j] != "") {
					$("#list_" + i + " option[value='" + this.activeList[j] + "']").attr("disabled","disabled");
				}
			}
		}
	};

	/* Bind all the events we expect to use */
	this.bindEvents = function() {
		// Select list change
		this.listWrapEl.on('change','.' + this.model.listClass,jQuery.proxy(this,"listChangeHandler"));
	};

	/* List change handler */
	this.listChangeHandler = function(e) {
		var itemId = $(e.currentTarget).val();
		var selectIdStr = $(e.currentTarget).attr('id');
		var selectId = selectIdStr.replace('list_','');
		// check blank
		if (itemId == '') {
			// Clear selected result box
			$(document).trigger('CLEAR_RESULT', [selectId, itemId] );
			// Clear Selection
			this.activeList[selectId-1] = '';
		} else {
			// Update result box id with new item
			$(document).trigger('UPDATE_RESULT', [selectId, itemId ] );
			// Mark selection
			this.activeList[selectId-1] = itemId;
		};
		// Update selects to block active
		this.refreshListData();
	};

	/* Lets roll */
	this.init(Model);	
};
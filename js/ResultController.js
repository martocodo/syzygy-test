/* Result controller - to manage showing and clearing results */
ResultController = function(Model) {
	/* Declare vars */
	this.model;
	this.resultWrapEl;

	/* Initial functions and params setting */
	this.init = function(Model) {
		// Set elements
		this.model 			= Model;
		this.resultWrapEl 	= $(this.model.resultWrapId);
		// Add our html elements
		this.addResultBoxes();
		// Bind our events to custom functions
		this.bindEvents();
	};

	/* Construct list elements */
	this.addResultBoxes = function() {

		for (i=1; i<=this.model.totalLists; i++) {
			var html = '<div class="col"><div id="result_' + i + '" class="' + this.model.resultClass + '"></div></div>';
			this.resultWrapEl.append(html);
		};
	};

	/* Bind all the events we expect to use */
	this.bindEvents = function() {
		// Listen for document events to change results
		$(document).on('UPDATE_RESULT',jQuery.proxy(this,"updateResultHandler"));
		$(document).on('CLEAR_RESULT',jQuery.proxy(this,"clearResultHandler"));
	};

	/* List change handler */
	this.updateResultHandler = function(e,listId,itemId) {
		var item 	= this.model.items[itemId];
		var html 	= '';
		// Loop through item object and show
		for (var spec in item) {
			// Only if required spec
			if (spec != 'active' && spec != 'title') {
				// 
				html += '<p class="spec">' + this.formatLabel(spec) + ' : ' + this.formatValue(spec,item[spec]) + '</p>';
			}
		}
		$('#result_' + listId).html(html);
		$('#result_' + listId).addClass('visible');
		// Show results title if not already shown
		if ($('#resultTitle').css('display') == 'none') {
			$('#resultTitle').css('display','block');
		};
	};

	/* List change handler */
	this.clearResultHandler = function(e,listId,itemId) {
		$('#result_' + listId).html('');
		$('#result_' + listId).removeClass('visible');
		// Hide if no result boxes visible
		if(!$('.' + this.model.resultClass).hasClass('visible')) {
			$('#resultTitle').css('display','none');
		}
	};

	/* Utility functions */

	/* Format for spec labels */
	this.formatLabel = function(str) {
		// Capitalise first letter
		var formattedStr = str.substr(0,1).toUpperCase() + str.substr(1);
		// Convert underscore to spaces
		formattedStr = formattedStr.replace('_',' ');
		return formattedStr;
	}

	/* Format for some spec values */
	this.formatValue = function(label,data) {
		
		var formattedValue;
		// Alter if required with symbols etc
		switch(label) {
			case 'price':
				formattedValue = this.model.currencySymbols[this.model.activeCurrency] + data;
			break;
			case 'processor_speed':
				formattedValue = data + ' GHz';
			break;
			case 'storage_capacity':
				formattedValue = data + ' GB';
			break;
			case 'ram':
				formattedValue = data + ' GB';
			break;
			case 'transport_weight':
				formattedValue = data + ' kg';
			break;
			case 'weight':
				formattedValue = data + ' kg';
			break;
			case 'dimensions':
				formattedValue = data + ' mm';
			break;
			default:
				formattedValue = data;
		}
		return formattedValue;
	};

	/* Lets roll */
	this.init(Model);	
};